#!/usr/bin/env sh 

# usage: ddc-switch-inputs 1/2
case "$1" in
    1)
        # conf 1: main
        OUT=("0x0f" "0x20")
        ;;
    2)
        # conf 2: vm
        OUT=("0x11" "0x21")
        ;;
    *)
        echo "Unknown input '$1'"
        exit 1
        ;;
esac

ddcutil --bus=8 setvcp 60 ${OUT[0]} &
ddcutil --bus=9 setvcp 60 ${OUT[1]} &
wait
