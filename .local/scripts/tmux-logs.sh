#!/bin/sh

# setup a custom tmux session called "general"
SESSION="general"
INTERFACE="enp67s0f1"

# create a new tmux session, starting a window named monitor
tmux new -s $SESSION -n monitor -d

# Select pane 1, then watch sensors with .5s interval
tmux send-keys -t 1 "watch -n 0.5 sensors" Enter 

# Open sensors in a horizontal split
tmux splitw -h -f
tmux send-keys "radeontop" Enter

# open empty terminal under radeontop
tmux splitw -v -l 50%

# create a new window called network and start iftop
tmux new-window -t $SESSION:2 -n network
tmux send-keys "sudo iftop -i $INTERFACE" Enter

# create a new window called news and start newsboat
tmux new-window -t $SESSION:3 -n news
tmux send-keys "newsboat" Enter

# return to monitor window
tmux select-window -t 1

# select terminal window
tmux selectp -t 3

# attach to session
tmux a -t $SESSION
