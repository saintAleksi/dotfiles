#!/bin/sh

# Set the target session
TARGET_SESSION="general"

# Get the current active window index and the list of available window indexes
CURRENT_WINDOW=$(tmux display-message -p -t "$TARGET_SESSION" '#{window_index}')
WINDOW_LIST=($(tmux list-windows -t "$TARGET_SESSION" -F '#{window_index}'))

# Find the position of the current window in the list and calculate the next window's position
CURRENT_POSITION=$(echo "${WINDOW_LIST[@]}" | tr ' ' '\n' | grep -n "^$CURRENT_WINDOW$" | cut -d: -f1)
NEXT_POSITION=$(( CURRENT_POSITION % ${#WINDOW_LIST[@]} ))

# Get the next window index from the list and switch to that window
NEXT_WINDOW=${WINDOW_LIST[$NEXT_POSITION]}
tmux select-window -t "$TARGET_SESSION:$NEXT_WINDOW"
