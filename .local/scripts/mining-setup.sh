#!/bin/sh

#TODO check defaults and apply here
POWER_CAP=250000000
FAN_SPEED=255
MEM_CLK=1000
CORE_CLK=1400
MINER=0
HWMON=$(ls /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/)

#TODO add an option for manual input of values
#TODO add a switch statement for switching between mining build and regular use

help(){
	echo -e "Set your AMD GPUs mem,core,fan,power\n"
	echo -e "syntax: mining_setup -[h|m|M|v] defaults to factory"
	echo -e "options:\nh print this help message"
	echo -e "m manual setup\nM mining setup\nv verbose"
}

while getopts ":hmMv:" option; do
	case $option in
        h) # display help
            help
            exit;;
        m) # manually set values
            #manual
            POWER_CAP=$OPTARG
            FAN_SPEED=$OPTARG
            MEM_CLK=$OPTARG
            CORE_CLK=$OPTARG;;
        M) # set values optimal for mining & start miner
            POWER_CAP=140000000
            FAN_SPEED=255
            MEM_CLK=1000
            CORE_CLK=1400
            MINER=1;;
            #Mining;;
        v) # verbose 
            # print shell output
            set -v;;
        \?)# invalid option
            echo "error invalid option"
            exit;;
	esac
done

#TODO add verbose option
# set power cap
sudo sh -c "echo $POWER_CAP > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/power1_cap"

#echo $power_cap

# enable manual fan control
sudo sh -c "echo 1 > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/pwm1_enable"

# set fan speed
sudo sh -c "echo $FAN_SPEED > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/pwm1"

# set memory clock
sudo sh -c "echo 'm 1 $MEM_CLK' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

# set core clock
sudo sh -c "echo 's 1 $CORE_CLK' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

# apply changes to clocks
sudo sh -c "echo 'c' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

if [ $MINER = 1 ]; then
	exec start-miner
else
	exit
fi
