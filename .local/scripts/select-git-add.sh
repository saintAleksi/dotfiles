#!/usr/bin/env sh

# interactively select files for git add
# used with dotfiles
# REQUIRES RIPGREP & FZF

#TODO aliases are deprecated in favor of functions, catch my drift?
alias conf='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# collect all files that have been modified, but not commited
# requires ripgrep, a superior grep written in rust n fzf, the fuzziest of finders
printfiles() {
    LIST=$(conf status | sed -n '/not staged for commit/,$p' \
    | rg "modified" | cut -d" " -f4)
    SELECTED=$(echo "$LIST" | fzf)
    addmore
}
addmore() {
    CONTINUE=$(echo -e "Add\nQuit" | fzf)
    if [ "$CONTINUE" = "Quit" ]; then
        conf add "$SELECTED" && exit
    else
        conf add "$SELECTED" || exit
        printfiles
    fi
}
printfiles
