#!/bin/sh

# set your desired mining/gaming settings
POWER_CAP=250000000
FAN_SPEED=255
MEM_CLK=1000
CORE_CLK=1400
HWMON=$(ls /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/)

#TODO add an option for manual input of values

#TODO help needs updating
help(){
	echo -e "Set your AMD GPUs mem,core,fan,power\n"
	echo -e "syntax: mining_setup -[h|m|M|v] defaults to factory"
	echo -e "options:\nh print this help message"
	echo -e "m manual setup\nM mining setup\nv verbose"
}

setclk(){
    sudo sh -c "echo 'c' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"
}
resetclk(){
    sudo sh -c "echo 'r' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"
}

power_cap(){
    sudo sh -c "echo $POWER_CAP > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/power1_cap"

}

fanctrl(){
    # enable manual fan control
    sudo sh -c "echo 1 > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/pwm1_enable"

    # set fan speed
    sudo sh -c "echo $FAN_SPEED > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/pwm1"
}

writeclks(){
    #Clock conversion (Mhz): (https://dri.freedesktop.org/docs/drm/gpu/amdgpu.html)
    #HBM: effective_memory_clock = memory_controller_clock * 1
    #G5:  effective_memory_clock = memory_controller_clock * 1
    #G6:  effective_memory_clock = memory_controller_clock * 2

    # set memory clock
    sudo sh -c "echo 'm 1 $MEM_CLK' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

    # set core clock
    sudo sh -c "echo 's 1 $CORE_CLK' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"
}

#TODO incorporate _dpm_sclk n _mclk to show active power state
printclks(){
    sudo sh -c "cat /sys/class/drm/card0/device/pp_od_clk_voltage"
}

while getopts "h:m:Mvrp" option; do
	case $option in
        h) # display help
            help
            exit;;
        m) # manually set values
            #manual
            POWER_CAP=$OPTARG
            FAN_SPEED=$OPTARG
            MEM_CLK=$OPTARG
            CORE_CLK=$OPTARG
            power_cap
            fanctrl
            writeclks
            setclk;;
        M) # set values optimal for mining & start miner
            POWER_CAP=140000000
            FAN_SPEED=255
            MEM_CLK=1000
            CORE_CLK=1400
            power_cap
            fanctrl
            writeclks
            setclk;;
        v) # verbose 
            # print shell output
            set -v;;
        r) # reset
            POWER_CAP=250000000
            power_cap
            resetclk;;
        p) # print current mem n core clks
            printclks;;
        \?)# invalid option
            echo "error invalid option"
            help
            exit;;
	esac
done
