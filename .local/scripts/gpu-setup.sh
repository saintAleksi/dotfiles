#!/bin/sh

#TODO check defaults and apply here
POWER_CAP=140000000
FAN_SPEED=255
MEM_CLK=1000
CORE_CLK=1400
MINER=0
HWMON=$(ls /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/)

#TODO add an option for manual input of values
#TODO add a switch statement for switching between mining build and regular use

print_out() {
    local MESSAGE="${@}"
    if [[ "${VERBOSE}" == true ]]; then
        echo "${MESSAGE}"
    fi
}

help(){
    echo -e "Set your AMD GPUs mem,core,fan,power\n"
    echo -e "syntax: gpu_setup.sh -[h|m|M|v] defaults to factory"
    echo -e "options:\nh print this help message"
    echo -e "m manual setup\nM mining setup\nv verbose"
}

while getopts ":hmMv" option; do
    case $option in
        h) # display help
            help
            exit
            ;;
        m) # manually set values
            echo "type in power cap, fan control, fan speed mem clk & core clk"
            read -r POWER_CAP FAN_CONTROL MEM_CLK CORE_CLK
            ;;
        M) # set values optimal for mining & start miner
            POWER_CAP=140000000
            FAN_SPEED=255
            MEM_CLK=1000
            CORE_CLK=1400
            MINER=1
            ;;
            #Mining;;
        v) # verbose 
            VERBOSE='true'
            print_out "verbose mode on"
            ;;
        \?)# invalid option
            echo "error invalid option"
            exit
            ;;
    esac
done
#shift "$((OPTIND-1))"

#TODO add verbose option
# set power cap
print_out "setting power cap to $POWER_CAP"
sudo sh -c "echo $POWER_CAP > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/power1_cap"

# enable manual fan control
#TODO
sudo sh -c "echo 1 > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/pwm1_enable"

# set fan speed
print_out "setting fan speed to $FAN_SPEED"
sudo sh -c "echo $FAN_SPEED > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/hwmon/$HWMON/pwm1"

# set memory clock
print_out "setting mem clk to $MEM_CLK"
sudo sh -c "echo 'm 1 $MEM_CLK' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

# set core clock
print_out "setting core clk to $CORE_CLK"
sudo sh -c "echo 's 1 $CORE_CLK' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

# apply changes to clocks
print_out "applying changes"
sudo sh -c "echo 'c' > /sys/devices/pci0000:20/0000:20:03.1/0000:21:00.0/0000:22:00.0/0000:23:00.0/pp_od_clk_voltage"

if [ $MINER = 1 ]; then
    exec /home/aleksi/.local/scripts/koe
else
    exit
fi
