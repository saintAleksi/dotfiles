" set colorscheme to seoul256
let g:seoul256_background = 233
colo seoul256

" add vim's own termdebug
packadd termdebug

" specify clang lib file location for clang_complete
let g:clang_library_path='/usr/lib64/libclang.so'

" map leader keys
let mapleader=' '
let maplocalleader=' '

" basic options {{{

" make pattern matching ignore case
set ignorecase
" override ignore case if pattern contains mixed case
set smartcase
" open splits more naturally: to the right n below
set splitbelow
set splitright
" when indenting with '>', use 4 spaces
set shiftwidth=4
" round indent to next multiple of 'shiftwidth'
set shiftround
" use space chars instead of tabs
set expandtab
" tab width to 4 columns
set tabstop=4
" enable hybrid line numbers
set number relativenumber
" correct lightline behaviour
set laststatus=2
" disable mode info
set noshowmode
" enable incremental search highlighting
set incsearch
" enable search highlighting
set hlsearch
" show partial cmd on screen
set showcmd
" set indent as the default fold method
set foldmethod=indent
" unset compatible
set nocp
" set updatetime to .3s, coc benefits from faster update
set updatetime=300
" define how backspace works
set backspace=indent,eol,start
" hide buffers instead of discarding
set hidden

" enable syntax highlighting
syntax enable
" enable file type detection
filetype on
" enable plugins and load plugin for detected file type 
filetype plugin on
" load indent file for detected file type
filetype indent on
" 80 char line
set textwidth=0
if exists('&colorcolumn')
    set colorcolumn=80
endif

" }}}

" highlightyank modifiers {{{ 

" set highlight duration for yanks
let g:highlightedyank_highlight_duration  = 500
" disable highlighting while in visual mode
let g:highlightedyank_highlight_in_visual = 0

" }}}

" lightline config {{{

let g:lightline = {
    \ 'colorscheme': 'seoul256',
    \ 'active': {
    \ 	'left': [ [ 'mode', 'paste' ],
    \		  [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component_function': {
    \	'gitbranch': 'FugitiveHead'
    \ },
    \ }

" }}}

" netrw config {{{
    
    " set style to tree
    let g:netrw_liststyle = 3
    " sync current dir n browsing dir
    let g:netrw_keepdir   = 0
    " set window size to % from whole
    let g:netrw_winsize   = 30
    " hide banner, can be shown interactively with I inside netrw
    let g:netrw_banner    = 0

" }}}

" coc config {{{

" set menu completion colors
hi CocMenuSel ctermfg=95

let g:coc_global_extensions=[ 'coc-git', 'coc-dash-complete', 'coc-dot-complete',
    \ 'coc-just-complete', 'coc-fzf-preview', 'coc-json', 'coc-rust-analyzer',
    \ 'coc-sh', 'coc-vimlsp', 'coc-toml', 'coc-yaml', 'coc-xml', 'coc-highlight',
    \ 'coc-omnisharp', 'coc-tsserver', 'coc-lua', 'coc-html' ]
" for coc-omnisharp to work, you need to install the dotnet SDK.
" for instructions, see here:
" https://docs.microsoft.com/en-us/dotnet/core/install
" on Arch Linux you can just run pacman -S dotnet-sdk
" after it is installed, run this command to install csharp language server:
" dotnet tool install --global csharp-ls 
" then follow the instructions on screen, specifically adding
" $HOME/.dotnet/tools to your PATH variable

" }}}

" mappings {{{

" need to run 'npm install -g livedown' if running for the 1st time
" toggle livedown server for markdown preview
nmap gm :LivedownToggle<cr>

" toggle netrw
nnoremap <Leader>n :Lexplore<cr>

" buffer navigation
nnoremap ]b :bnext<cr>
nnoremap [b :bprev<cr>

" tab navigation
nnoremap ]t :tabn<cr>
nnoremap [t :tabp<cr>

" enable completion navigation with <C-j> n <C-k> for coc
inoremap <silent><expr> <C-j> coc#pum#visible() ? coc#pum#next(1) : "\<C-j>"
inoremap <silent><expr> <C-k> coc#pum#visible() ? coc#pum#prev(1) : "\<C-k>"

" enable leader hjkl navigation between splits
nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l

" shebang
inoreabbrev <expr> #!! "#!/usr/bin/env" . (empty(&filetype) ? '' : ' '.&filetype)

" }}}

" vimscript {{{

" autocmds for vim files 
augroup filetype_vim
    autocmd!
    autocmd Filetype vim setlocal foldmethod=marker
augroup END

" }}}

" fzf config {{{

" include fzf in runtimepath
set rtp+=~/.fzf

" all files
command! -nargs=? -complete=dir AF
    \ call fzf#run(fzf#wrap(fzf#vim#with_preview({
    \   'source': 'fd --type f --hidden --follow --exclude .git --no-ignore . '.expand(<q-args>)
    \ })))

command! -bar MoveBack if &buftype == 'nofile' && (winwidth(0) < &columns / 3
    \ || winheight(0) < &lines / 3) | execute "normal! \<c-w>\<c-p>" | endif
nnoremap <silent> <Leader><Enter>  :MoveBack<bar>Files<cr>
nnoremap <silent> <Leader><Leader> :MoveBack<bar>Buffers<cr>
nnoremap <silent> <Leader>L        :Lines<cr> 

" }}} 
